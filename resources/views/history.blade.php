<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <title>testparser</title>     
		
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                   <a href="http://testparser/public">Главная</a>
                </div>
			</div>   
			</br>	
			<div class="row">
				<div class="col-md-12">
					{{ $data->links() }}
				</div>
			</div>			
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>№</th>
								<th>Domain</th>
								<th>KeyWord</th>
								<th>Position</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($data as $row)
							<tr>
								<td>{{$row->id}}</td>
								<td>{{$row->domain}}</td>
								<td>{{$row->keyWord}}</td>
								<td>{{$row->position}}</td>
								<td>{{$row->created_at}}</td>
								
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			
        </div>
    </body>
	
</html>
