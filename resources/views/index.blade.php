<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <title>testparser</title>     
		
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Выборка результатов из Google</h1>
                </div>
			</div>
			<div class="row">
                <div class="col-md-12">
                   <a href="http://testparser/public/history">История</a>
                </div>
			</div>
			<div class="row">
                <div class="col-md-12">
                    <form role="form">
                        <div class="form-group">
                            <label for="inputDomain">Доменное имя</label>
                            <input type="text" class="form-control" id="domain" placeholder="Доменное имя" required="true" name="domain">
                        </div>
                        <div class="form-group">
                            <label for="inputWord">Ключевое слово</label>
                            <input type="text" class="form-control" id="keyWord" placeholder="Введите ключевое слово" required="true" name="keyWord">
                        </div>
                        
                        <button type="submit" class="btn btn-default">Найти</button>
                    </form>
                </div>
            </div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>№</th>
								<th>Domain</th>
								<th>KeyWord</th>
								<th>Position</th>
								<th>Date</th>
							</tr>
						</thead>
						<tbody>
               
						</tbody>
					</table>
				</div>
			</div>
        </div>
    </body>
	<script>
		$(document).ready(function(){
			$.ajaxSetup({
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
						});
			
			$('table').hide();
			
			$('button').click(function(e){
				e.preventDefault();
				
				$.ajax({
					type: "POST",
					url: "http://testparser/public/ajax",
					
					data: { domain: $('#domain').val(), 
							keyWord:$('#keyWord').val()},
					success: function(data) {
						let ans=JSON.parse(data);
						console.log(ans);
						$('table').show();
						let el=`<tr><td>${ans.id}</td><td>${ans.domain}</td><td>${ans.keyWord}</td><td>${ans.position}</td><td>${ans.created_at}</td></tr>`;
						$('table').prepend(el);
						},
					error: function() {
						alert('There was some error performing the AJAX call!');
						}
				})
			
			
		})
		})
	</script>
</html>
