<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Result;

class History extends Controller
{
    public function index(){
		$data=Result::orderBy('created_at','desc')->paginate(10);
		return view('history',['data'=>$data]);
	}
}
