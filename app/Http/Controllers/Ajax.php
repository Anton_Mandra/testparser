<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Result;

class Ajax extends Controller
{
     public function find(){
	   if ($_POST['domain']===null || $_POST['keyWord']===null){
		   die();
	   }else{
		   $keyWord = $_POST['keyWord'];
		   $keyWordEnc=urlencode($keyWord);
	       $target=$_POST['domain'];
		   $url ="http://www.google.com/search?q="
					. $keyWordEnc
					. "&aqs=chrome..69i57j0l5.5103j0j8&sourceid=chrome&ie=UTF-8&num=100";
           $html= file_get_contents($url);
           preg_match_all('/<div class="BNeawe UPmit AP7Wnd">(.+?) /is', $html, $matches);
           $urlArr= str_replace(['https://','http://','www.'],[], $matches[1]);
           $index= array_search($target, $urlArr);
		   if ($index!==false){
				$position=++$index;
				
				}else{
				$position='not found';
				
				}
			
			$result=new Result;
			$result->domain=addslashes(htmlspecialchars($target));
			$result->keyWord=addslashes(htmlspecialchars($keyWord));
			$result->position=$position;
			$result->save();
			$answer=Result::orderBy('created_at','desc')->first()->toJson();
			return $answer;
	   }
   }
}
